# converter-temp

## Project setup
```
npm install
```

### Run the project
```
npm start
```

### Endpoint docs

**GET ALL DATA**
----
  Returns all json user data.

* **URL**

  /user_pages

* **Method:**

  `GET`
  
*  **URL Params**

   **optional**
   `limit=[INTEGER]`
   `offset=[INTEGER]`

* **Data Params**
   
   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    [
      {
        "id": "0000436c",
        "column1": null,
        "category_id": null,
        "name": "Landing Page",
        "descriptions": "Landing page.",
        "status_id": "b6eede2a-9b63",
        "images": "https://img.imageboss.me",
        "json_data": {},
        "slug": "landing-page",
        "index": 1,
        "created_at": "2020-02-07T04:17:40.015Z",
        "created_by": null,
        "updated_at": "2020-02-15T17:58:21.983Z",
        "updated_by": null
      }
    ]
    ```
 
* **Error Response:**

  * **Code:** 400 SERVER ERROR <br />


**GET DATA AND CREATE IN LOCAL**
----
  Create json files in local folder.

* **URL**

  /user_pages/write

* **Method:**

  `GET`
  
*  **URL Params**

   **optional**
   `limit=[INTEGER]`
   `offset=[INTEGER]`

* **Data Params**
   
   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "msg": "done get data and save in local folder"
    }
    ```
 
* **Error Response:**

  * **Code:** 400 SERVER ERROR <br />


**CONVERT DATA TO NEW STRUCTURE**
----
  Get data and convert it to new structure and save in local folder.

* **URL**

  /user_pages/convert

* **Method:**

  `GET`
  
*  **URL Params**

   **optional**
   `limit=[INTEGER]`
   `offset=[INTEGER]`

* **Data Params**
   
   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "msg": "done convert process and save in local folder"
    }
    ```
 
* **Error Response:**

  * **Code:** 400 SERVER ERROR <br />


**CONVERT AND UPDATE DATA IN DATABASE**
----
  Get data, convert it to new structure and update the database.

* **URL**

  /user_pages/update

* **Method:**

  `PUT`
  
*  **URL Params**

   **optional**
   `limit=[INTEGER]`
   `offset=[INTEGER]`

* **Data Params**
   
   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "msg": "done process update"
    }
    ```
 
* **Error Response:**

  * **Code:** 400 SERVER ERROR <br />


**SEEDING DATA**
----
  Seeding all json data from local folder to database.

* **URL**

  /user_pages

* **Method:**

  `POST`
  
*  **URL Params**

* **Data Params**
   
   None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```json
    {
      "msg": "done seeding data to database"
    }
    ```
 
* **Error Response:**

  * **Code:** 400 SERVER ERROR <br />
