const express = require("express")
const app = express();
const PORT = 3000;

const routes = require("./router");

app.use(express.urlencoded({extended: false}))
app.use(express.json())

app.use(routes)

app.listen(PORT, () => {
  console.log("listening to ", PORT)
})
