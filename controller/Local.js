const path = require("path");
const dirPath = path.join(__dirname, "../data/raw");
const targetPath = path.join(__dirname, "../data/doneProcessed");
const { readTemplateDirectory, checkStructureV2 } = require("../helper/checkStructure");

class Local {
	static async checkStructure(req, res, next) {
		async function loopingData(files) {
			for(const file of files) {
				// console.log(`CHECK FILE ${file.name}`);
				await checkStructureV2(file.json_data);
			}
		}
		let type = req.params.type;
		let { limit, offset } = req.query;
		if (type) {
			let data
			if (type === "raw") {
				data = readTemplateDirectory(dirPath, limit, offset);
			} else if (type === "done") {
				data = readTemplateDirectory(targetPath, limit, offset);
			}
			
			await loopingData(data, limit, offset)
			res.status(200).json(`DONE CHECK STRUCTURE ${type}`);
			console.log("DONE CHECK STRUCTURE");
		} else {
			res.status(500).json("WRONG ENDPOINT");
		}
	}
}

module.exports = Local;