const fs = require("fs");
const path = require("path");
const dirPath = path.join(__dirname, "../data/raw");
const targetPath = path.join(__dirname, "../data/doneProcessed");
const convertedPath = path.join(__dirname, "../data/removeNestedRowColumn");
const { user_page } = require("../models");
const converter = require("../helper/converter");
const converterNestedRowColumn = require("../helper/converter2");

class UserPagesCopyController {
  // SEEDING FROM LOCAL TO DATABASE
  static async seedingData(req, res, next) {
    let allTemplate = fs.readdirSync(dirPath, "utf8");
    allTemplate.sort((a, b) => parseInt(a) - parseInt(b));
    try {
      await allTemplate.map(async (el, index) => {
        const payload = JSON.parse(fs.readFileSync(`${dirPath}/${el}`));
        await user_page.create(payload);
      })
      res.status(200).json({ msg: "done seeding data to database" });
    } catch(err) {
      res.status(400).json(err);
    }
  }

  // GET ALL DATA FROM DATABASE 
  static getAllData(req, res, next) {
    const limit = req.query.limit;
    const offset = parseInt(req.query.offset);
    let parameter = { order: [["id", "asc"]] };
    if (limit >= 0 && offset >= 0) {
      parameter = { limit, offset, order: [["id", "asc"]] };
    }
    user_page.findAll(parameter)
      .then(data => {
        res.status(200).json(data);
      })
      .catch(err => {
        console.log(err);
        res.status(400).json(err);
      })
  }

  // GET LIMITED DATA WITH OFFSET AND SAVE DATA TO LOCAL FOLDER
  // /user_pages/write?limit=your_limit&offset=your_offset
  static getDataLimitOffsetAndCreate(req, res, next) {
    const limit = req.query.limit ? req.query.limit : 1;
    const offset = req.query.offset ? parseInt(req.query.offset) : 0;
    let parameter = { order: [[ "id", "asc" ]] };
    if (limit > 0 && offset >= 0) {
      parameter = { limit, offset, order: [[ "id", "asc" ]] };
    }
    user_page.findAll(parameter)
      .then(data => {
        console.log("BEGIN GET DATA");
        data.forEach((el, index) => {
          console.log("START", el.name);
          if (!fs.existsSync(dirPath)) {
            fs.mkdirSync(dirPath);
          }
          if (typeof el.json_data === "string") {
            el.json_data = JSON.parse(el.json_data);
          }
          const newData = { ...el };
          fs.writeFileSync(`${dirPath}/${index+1+offset}-${el.id}-${el.slug}.json`, JSON.stringify(newData.dataValues, null, 2), "utf8");
          console.log("WRITE", el.name);
        })
        res.status(200).json({ msg: "done get data and save in local folder" });
      })
      .catch(err => {
        console.log(err);
        res.status(400).json(err);
      })
  }

  // GET LIMITED DATA WITH OFFSET AND CONVERT DATA TO NEW STRUCTURE AND SAVE DATA TO LOCAL FOLDER
  // /user_pages/convert?limit=your_limit&offset=your_offset
  static async convertAndWrite(req, res, next) {
    const limit = req.query.limit ? req.query.limit : 0;
    const offset = req.query.offset ? parseInt(req.query.offset) : 0;
    let parameter = { order: [["id", "asc"]] };
    if (limit > 0 && offset >= 0) {
      parameter = { limit, offset, order: [["id", "asc" ]]};
    }
    try {
      const data = await user_page.findAll(parameter);
      converter(data, offset);
      res.status(200).json({ msg: "done convert process and save in local folder" });
    } catch(err) {
      console.log(err);
      res.status(400).json(err);
    }
  }

  // GET LIMITED DATA WITH OFFSET AND CONVERT DATA TO NEW STRUCTURE AND UPDATE DATABASE
  static convertAndUpdate(req, res, next) {
    const limit = req.query.limit ? req.query.limit : 1;
    const offset = req.query.offset ? parseInt(req.query.offset) : 0;
    let parameter = { order: [["id", "asc"]] };
    if (limit > 0 && offset >= 0) {
      parameter = { limit, offset, order: [["id", "asc"]] };
    }
    user_page.findAll(parameter)
      .then(data => {
        console.log("BEGIN CONVERT");
        let promises = [];
        let newData = converter(data, offset, "update");
        newData.forEach(el => {
          promises.push(user_page.update(el.dataValues, { where: { id: el.dataValues.id } }))
        });
        return Promise.all(promises);
      })
      .then(data => {          
        res.status(200).json({ msg: "done process update" });
      })
      .catch(err => {
        console.log(err)
        res.status(400).json(err);
      }) 
  }

  static async changeStructureRowColumn(req, res, next) {
    let allTemplate = fs.readdirSync(targetPath, "utf8");
    allTemplate.sort((a, b) => parseInt(a) - parseInt(b));
    try {
      await allTemplate.map(async (el, index) => {
        const payload = JSON.parse(fs.readFileSync(`${targetPath}/${el}`));
        converterNestedRowColumn(payload)
      })
      res.status(200).json({ msg: "done change structure row column from doneprocessed" });
    } catch(err) {
      res.status(400).json(err);
    }
  }
}


module.exports = UserPagesCopyController;