const names = [
	"Box",
	"Button",
	"Column",
	"Countdown",
	"Counter",
	"Counter",
	"Divider",
	"Embed",
	"Image",
	"Navigation",
	"NavigationChild",
	"Root",
	"Row",
	"Section",
	"Slider",
	"SliderChild",
	"Spacer",
	"Text",
	"Video",
	// Wrong Name
	"SlideBox", // SliderBox
	"SliderContent",
	"Paragraph", // Text
	"SliderChildEditor",
	"VideoEditor", //Video
	"EmptyBox",
	"IconBlankSection"
];
const types = [
	"BoxContent",
	"ButtonContent",
	"ColumnContent",
	"CountdownContent",
	"CounterContent",
	"DividerContent",
	"EmbedContent",
	"ImageContent",
	"NavigationContent",
	"NavigationChildContent",
	"RootContent",
	"RowContent",
	"SectionContent",
	"SliderContent",
	"SliderChildContent",
	"SpacerContent",
	"TextContent",
	"VideoContent",
	// New Type
	"SliderBoxContent",
	// Wrong Type
	"DivContent",
	"Slider",
	"SliderChild"
];
const editors = [
	"BlankEditor",
	"BoxEditor",
	"ButtonEditor",
	"ColumnEditor",
	"CountdownEditor",
	"CounterEditor",
	"DividerEditor",
	"EmbedEditor",
	"ImageEditor",
	"NavigationEditor",
	"NavigationChildEditor",
	"RootEditor",
	"RowEditor",
	"SectionEditor",
	"SliderEditor",
	"SliderChildEditor",
	"SpacerEditor",
	"TextEditor",
	"VideoEditor",
	// New Editor
	"BackgroundEditor",
	"SliderBoxEditor",
	// Wrong Editor
	"Root",
	"SliderContent"
];

const checkStructure = (data, parentType = []) => {
  parentType.push(data.name)
  // RECURSIVE FUNCTION
  if (data && data.child.length !== 0) {
    let i = 0
    while(data.child && i !== data.child.length) {
      checkStructure(data.child[i], parentType);
      i++
    }
  }
  if (data.child.length === 0) {
    let boxIndex = parentType.lastIndexOf("Box");
    if (
      parentType[0] !== "Root" ||
      parentType[1] !== "Section" ||
      parentType[2] !== "Box" ||
      (parentType[3] && parentType[3] !== "Row") ||
      (parentType[4] && parentType[4] !== "Column") ||
      boxIndex > 2
      // (parentType[2] === "Box" && parentType[3] === "Row" && parentType[4] === "Column" && parentType[5] === "Row" && parentType[6] === "Column") ||
      // (parentType[2] === "Box" && parentType[3] === "Row" && parentType[4] === "Column" && parentType[5] === "Column")
      // (parentType[2] === "Row" && parentType[3] === "Column" && parentType[4] === "Row" && parentType[5] === "Column" && !parentType[6]) ||
      // (parentType[1] === "Slider" && parentType[3] === "SliderContent")
    ) {
      console.log("FOUND ERROR", parentType);
    }
    // console.log(parentType);
  }
  parentType.pop();
}

const checkStructureV2 = (data, parentNode = []) => {
	const { name, type, editor } = data;
	const contentData = { name, type, editor };
	parentNode.push(contentData);
	if (!names.includes(contentData.name)) {
		console.log(contentData.name, "name");
	}
	if (!types.includes(contentData.type)) {
		console.log(contentData.type, "type")
	}
	if (!editors.includes(contentData.editor)) {
		console.log(contentData.editor, "editor");
	}
	if (data && data.child.length !== 0) {
		let i = 0;
		while(data.child && i !== data.child.length) {
			checkStructureV2(data.child[i], parentNode);
			i++;
		}
	}
	parentNode.pop();
}



const fs = require("fs");

// READ TEMPLATE AND ASSIGN TO VARIABLE
function readTemplateDirectory(path, limit = 0, offset = 0) {
	console.log("READ TEMPLATE DIRECTORY START");
  let data = [];
  let allTemplate = fs.readdirSync(path, "utf8");
  allTemplate.forEach((el, i) => {
		if (el.includes("json")) {
			if (limit && offset) {
				if (i > offset && i <= limit) {
					data.push(JSON.parse(fs.readFileSync(`${path}/${el}`)));
				}
			} else if(limit && !offset) {
				if (i <= limit) {
					data.push(JSON.parse(fs.readFileSync(`${path}/${el}`)));
				}
			} else if(!limit && offset) {
				if (i > offset) {
					data.push(JSON.parse(fs.readFileSync(`${path}/${el}`)));
				}
			} else {
				data.push(JSON.parse(fs.readFileSync(`${path}/${el}`)));
			}
		}
	})
	console.log("READ TEMPLATE DIRECTORY FINISH");
  return data;
}

module.exports = {
	readTemplateDirectory,
	checkStructure,
	checkStructureV2
}