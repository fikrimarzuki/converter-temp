const recursiveFunction = (data, parentType = []) => {
  data = changeNameTypeEditor(data); // 1.

  // 2.
  if (data.type === "SectionContent") {
    data.dataLayout.properties.style = changeStyleSection(data.dataLayout.properties.style);
  }

  if (data.type === "RowContent") {
    data.dataLayout.properties = changePropertiesRow(data.dataLayout.properties);
  }

  if (data.type === "ColumnContent") {
    const { result, gridProperties } = changeClassCol(data.dataLayout.properties.class);
    data.dataLayout.properties.style = changeStyleCol(data.dataLayout.properties.style);
    data.dataLayout.properties.class = result;
    if (gridProperties && Object.entries(gridProperties).length !== 0) {
      data.dataLayout = { ...data.dataLayout, gridProperties };
    }
  }

  if (data.type === "ImageContent") {
    data.dataLayout.properties.style = changeStyleImage(data.dataLayout.properties.style);
  }

  if (data.type === "TextContent") {
    data.dataLayout.properties = changePropertiesText(data.dataLayout.properties);
  }

  // 3.
  if (data && data.type) parentType.push(data.type);

  data = changeStructure(data, parentType)
  if (!data) {
    return data;
  }

  // 4.
  if (data && data.child.length !== 0) {
    let i = 0;
    // 5.a
    while(data.child && i !== data.child.length) {
      // 5.b
      data.child[i] = recursiveFunction(data.child[i], parentType);
      if (!data.child[i]) {
        data.child.splice(i, 1);
        i--;
      }
      i++;
    }
  }

  data = sortObject(data); // Sort Descending Object Keys
  parentType.pop(); // 6.
  // 7.
  return data;
}

function changeNameTypeEditor(data) {
  if (data) {
    if (data.editor === "") {
      data.editor = "BlankEditor"
    }
    if (data.type === "Root") {
      data.type = "RootContent";
    } else if (data.type === "DivContent" && data.name === "Section") {
      data.type = "SectionContent"
      data.editor = "SectionEditor"
    } else if (data.type === "DivContent" && data.name === "Box" || data.name === "box") {
      data.name = "Box";
      data.type = "BoxContent";
      data.editor = "BoxEditor";
    } else if (data.type === "Row") {
      data.type = "RowContent";
      data.editor = "RowEditor";
    } else if (data.type === "Column") {
      data.type = "ColumnContent";
      data.editor = "ColumnEditor"
    } else if (data.type === "TextContent") {
      data.name = "Text";
    } else if (data.name === "VideoEditor") {
      data.name = "Video";
    } else if (data.type === "Slider") {
      data.type = "SliderContent";
    } else if (data.type === "SliderChild") {
      data.name = "SliderChild";
      data.type = "SliderChildContent";
    } else if (data.editor === "SliderContent") {
      data.name = "SlideBox";
      data.type = "SliderBoxContent";
      data.editor = "SliderBoxEditor";
    }
  }
  return data;
}

function changeClassCol(data) {
  const colRegex = new RegExp(/(col(s|\-md|\-lg))/gm);
  const offsetRegex = new RegExp(/(offset(\-md|\-lg|))/gm);
  const orderRegex = new RegExp(/(order(\-md|\-lg|))/gm);

  let result = [];
  let gridProperties = {
    col: { xs: 12, md: "", lg: "" },
    offset: { xs: "", md: "", lg: "" },
    order: { xs: "", md: "", lg: "" }
  };
  if (data && Array.isArray(data) && data.length) {
    data.forEach(el => {
      let isChange = true;
      let dataConverted = "";
      if (el.search(colRegex) === 0) {
        dataConverted = el.split(colRegex)[1]
      } else if (el.search(offsetRegex) === 0) {
        dataConverted = el.split(offsetRegex)[1]        
      } else if (el.search(orderRegex) === 0) {
        dataConverted = el.split(orderRegex)[1]
      } else {
        isChange = false;
      }
      if (isChange) {
        const dataSplit = el.split("-")
        if (dataSplit.length === 2) {
          if (dataSplit[0] === "cols" || dataSplit[0] === "col") {
            let cols = 12;
            if (parseInt(dataSplit[1])) {
              cols = parseInt(dataSplit[1]);
            }
            gridProperties.col.xs = cols;
          } else {
            gridProperties[dataSplit[0]].xs = parseInt(dataSplit[1]);
          }
        } else {
          if (dataSplit[1] === "sm") {
            isChange = false;
          } else {
            if (!dataSplit[1]) {
              gridProperties[dataSplit[0]] = ""
            } else {
              gridProperties[dataSplit[0]][dataSplit[1]] = parseInt(dataSplit[2])
            }
          }
        }
        result.push(dataConverted)
      }
    })
  };
  return { result, gridProperties };
}

function changeStyleSection(data) {
  if (data && Object.entries(data).length !== 0) {
    if (data.padding) delete data.padding;
  }
  data.paddingTop = "64px";
  data.paddingBottom = "64px";
  data.paddingLeft = "0px";
  data.paddingRight = "0px";
  return data;
}

function changePropertiesRow(data) {
  if (data) {
    if (data.style && Object.entries(data.style).length !== 0) {
      if (data.style.padding) delete data.style.padding;
    }
    if (data.class && data.class.length !== 0) {
      if (typeof data.class === "object") {
        data.class = []
      } else {
        data.class = data.class.filter(el => {
          if (!["m-0", "text-center", "text-lg-left", "text-left"].includes(el)) {
            return el
          }
        })
      }
    }
  }
  return data;
}

function changeStyleCol(data) {
  if(data.padding) {
    let padding = data.padding;
    if (data.padding.includes("em") || (data.paddingTop && data.paddingTop.includes("em"))) {
      padding = "16px";
    }
    data.paddingTop = padding;
    data.paddingBottom = padding;
    data.paddingLeft = padding;
    data.paddingRight = padding;
    delete data.padding;
  }
  return data;
}

function changeStyleImage(data) {
  if (data && Object.entries(data).length !== 0) {
    if (data.width) {
      data.maxWidth = data.width;
      delete data.width;
    }
    if (data.height) {
      data.maxHeight = data.height;
      delete data.height;
    }
  }
  return data;
}

function changePropertiesText(data) {
  if (data) {
    if (data.style && Object.entries(data.style).length !== 0) {
      if (data.style.marginTop) delete data.style.marginTop;
    }
    if (data.class && data.class.length !== 0) {
      data.class = []
    }
  }
  return data;
}

function changeStructure(data, parentType) {
  if (data.name === "Section" || parentType[1] === "SectionContent") {
    data = changeStructureSection(data, parentType);
  } else {
    data = changeStructureSlider(data, parentType);
  }
  return data;
}

function changeStructureSection(data, parentType) {
  let hasChild = false;
  const boxDefaultStyle = {
    maxWidth: "1170px",
    paddingTop: "0px",
    paddingBottom: "0px",
    paddingLeft: "0px",
    paddingRight: "0px",
    margin: "0 auto",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  }
  const rowContent = {
    name: "Row",
    type: "RowContent",
    editor: "RowEditor",
    dataLayout: {
      properties: {
        class: [],
        style: {
          paddingTop: "16px",
          paddingBottom: "16px",
          paddingLeft: "16px",
          paddingRight: "16px"
        }
      }
    },
    child: []
  }
  const colContent = {
    name: "Column",
    type: "ColumnContent",
    editor: "ColumnEditor",
    dataLayout: {
      properties: {
        class: [],
        style: { 
          paddingTop: "16px",
          paddingBottom: "16px",
          paddingLeft: "16px",
          paddingRight: "16px"
        }
      },
      gridProperties: {
        col: { xs: 12, md: "", lg: "" },
        offset: { xs: "", md: "", lg: "" },
        order: { xs: "", md: "", lg: "" }
      }
    },
    child: []
  }
  if (!data.child) {
    return null;
  }
  if (data.child.length !== 0) hasChild = true;
  if (parentType.length === 2) {
    // In Section, if section have child, wrap the child with Box and place the child in Box child, else add Box with no child
    if (hasChild) {
      let filter = data.child.filter(el => el.name === "Box");
      let boxIndex = data.child.findIndex(el => el.name === "Box");
      let childFilter = data.child.filter(el => el.name !== "Box");
      if (!filter.length && childFilter.length === data.child.length) {
        data = {
          ...data,
          child: [
            {
              uniqueID: uniqid(),
              name: "Box",
              type: "BoxContent",
              editor: "BoxEditor",
              dataLayout: {
                properties: {
                  class: [],
                  style: { ...boxDefaultStyle }
                }
              },
              child: [ ...data.child ]
            }
          ]
        }
      } else {
        data = {
          ...data,
          child: [
            {
              uniqueID: uniqid(),
              name: "Box",
              type: "BoxContent",
              editor: "BoxEditor",
              dataLayout: {
                properties: {
                  class: [],
                  style: { ...boxDefaultStyle }
                }
              },
              child: []
            }
          ]
        }
        let boxChild = filter.length ? filter[0].child : []
        if (boxIndex === 0) {
          data.child[0].child = [ ...boxChild, ...childFilter ]
        } else {
          data.child[0].child = [ ...childFilter, ...boxChild ]
        }
        // Section have 2 box ? Possible ? If not then Done for this
      }
    } else {
      data = {
        ...data,
        child: [
          {
            uniqueID: uniqid(),
            name: "Box",
            type: "BoxContent",
            editor: "BoxEditor",
            dataLayout: {
              properties: {
                class: [],
                style: { ...boxDefaultStyle }
              }
            },
            child: []
          }
        ]
      }
    }
  } else if(parentType.length === 3) {
    // Change Styling in BoxContent
    let boxFilter = data.child.filter(el => el.name === "Box");
    let childFilter = data.child.filter(el => el.name !== "Box");
    if (boxFilter.length) {
      let boxIndex = data.child.findIndex(el => el.name === "Box");
      if (boxFilter[0].dataLayout.properties.style.width === "10em") {
        if (boxFilter[0].child.length !== 0) {
          data = {
            ...data,
            child: [
              {
                uniqueID: uniqid(),
                ...rowContent,
                child: [
                  {
                    uniqueID: uniqid(),
                    ...colContent,
                    child: [ ...boxFilter[0].child ]
                  },
                  {
                    uniqueID: uniqid(),
                    ...colContent
                  },
                  {
                    uniqueID: uniqid(),
                    ...colContent
                  },
                  {
                    uniqueID: uniqid(),
                    ...colContent
                  }
                ]
              }
            ]
          }
          if (boxIndex === 0) {
            data.child.push(...childFilter);
          } else {
            data.child.unshift(...childFilter);
          }
        } else {
          data.dataLayout.properties.style = { ...boxDefaultStyle }
        }
      }
    }
    let filter = data.child.filter(el => el.type === "RowContent");
    if (hasChild && !filter.length) {
      data = {
        ...data,
        child: [
          {
            uniqueID: uniqid(),
            ...rowContent,
            child: [
              {
                uniqueID: uniqid(),
                ...colContent,
                child: [ ...data.child ]
              }
            ]
          }
        ]
      }
    } else {
      data.dataLayout.properties.style = { ...boxDefaultStyle }
    }
  } else if (parentType.length > 3 && parentType.length % 2 === 0) {
    if (parentType.length === 4) {
      if (data.type === "BoxContent") {
        parentType.splice(parentType.length-1, 1, "RowContent");
        if (hasChild) {
          data = {
            uniqueID: uniqid(),
            ...rowContent,
            child: [ ...data.child ]
          }
        } else {
          data = {
            uniqueID: uniqid(),
            ...rowContent,
            child: [
              {
                uniqueID: uniqid(),
                ...colContent
              }
            ]
          }
        }
      } else if(data.type !== "RowContent") {
        parentType.splice(parentType.length-1, 1, "RowContent");
        data = {
          uniqueID: uniqid(),
          ...rowContent,
          child: [ data ]
        }
      }
    } else {
      // STILL PROBLEM, ADD ROW COLUMN AGAIN - DELETE BOX - IF CHILD = COMPONENT DONE, ELSE IF 
      if (data.type === "BoxContent") {
        if (hasChild) {
          parentType.splice(parentType.length-1, 1, "RowContent");
          // cek child, if child have child, do something
          // cek child, if child doesnt have child, replace data with child
          // But what if child length is more than 1 ????
          data = {
            uniqueID: uniqid(),
            ...rowContent,
            child: [ ...data.child ]
          }
        }  else {
          parentType.pop();
          data = null;
        }
      } else if (data.type === "ColumnContent") {
        parentType.splice(parentType.length-1, 1, "RowContent");
        data = {
          uniqueID: uniqid(),
          ...rowContent,
          child: [ data ]
        }
      }
    }
  } else if (parentType.length > 4 && parentType.length % 2 === 1) {
    if (data.type === "ColumnContent" && data.child.length === 1 && data.child[0].name === "Box") {
      data = {
        ...data,
        child: [ ...data.child[0].child ]
      }
    } else if (data.type === "BoxContent") {
      parentType.splice(parentType.length-1, 1, "ColumnContent");
      if (hasChild) {
        data = {
          uniqueID: uniqid(),
          ...colContent,
          child: [ ...data.child ]
        }
      } else {
        data = {
          uniqueID: uniqid(),
          ...colContent
        }
      }
    } else if (data.type !== "ColumnContent" && data.type !== "NavigationChildContent") {
      parentType.splice(parentType.length-1, 1, "ColumnContent");
      data = {
        uniqueID: uniqid(),
        ...colContent,
        child: [ data ]
      }
    }
  }
  return data;
}

function changeStructureSlider(data, parentType) {
  return data;
}

function sortObject(data) {
  const ordered = {};
  Object.keys(data).sort().reverse().forEach(key => {
    ordered[key] = data[key];
  });
  return ordered;
}


const fs = require("fs");
const path = require("path");
const rawPath = path.join(__dirname, "../data/raw");
const processedPath = path.join(__dirname, "../data/doneProcessed");
const uniqid = require("uniqid");

function converting(data, offset, type) {
  offset = offset ? offset : 0;
  let dataProcessed = [];
  data.forEach((el, index) => {
    console.log("BEGIN", el.name);
    if (typeof el.json_data === "string") {
      el.json_data = JSON.parse(el.json_data);
    }
    let newJson = recursiveFunction(el.json_data);
    if (type === "update") {
      dataProcessed.push({ ...el, json_data: newJson });
    } else {
      fs.writeFileSync(`${processedPath}/${index+1+offset}-${el.id}-${el.slug}.json`, JSON.stringify(newJson, null, 2), "utf8");
    }
    console.log("DONE", el.name);
  })
  console.log("======= FINISH =======");
  return dataProcessed;
}

module.exports = converting;