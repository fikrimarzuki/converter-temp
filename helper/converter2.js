const recursiveFunction = (data, parentType = []) => {
  if (data && data.type) parentType.push(data.type);

  data = changeStructure(data, parentType)
  if (!data) {
    return data;
  }

  // 4.
  if (data && data.child.length !== 0) {
    let i = 0;
    // 5.a
    while(data.child && i !== data.child.length) {
      // 5.b
      data.child[i] = recursiveFunction(data.child[i], parentType);
      if (!data.child[i]) {
        data.child.splice(i, 1);
        i--;
      }
      i++;
    }
  }

  data = sortObject(data); // Sort Descending Object Keys
  parentType.pop(); // 6.
  // 7.
  return data;
}

function changeStructure(data, parentType) {
  console.log(parentType);
  return data;
}

function sortObject(data) {
  const ordered = {};
  Object.keys(data).sort().reverse().forEach(key => {
    ordered[key] = data[key];
  });
  return ordered;
}

const fs = require("fs");
const path = require("path");
const processedPath = path.join(__dirname, "../data/doneProcessed");
const uniqid = require("uniqid");

function convertNestedStructure(data, offset, type) {
  offset = offset ? offset : 0;
  let dataProcessed = [];
  data.forEach((el, index) => {
    console.log("BEGIN", el.name);
    let newJson = recursiveFunction(el.json_data);
    fs.writeFileSync(`${processedPath}/${index+1+offset}-${el.id}-${el.slug}.json`, JSON.stringify(newJson, null, 2), "utf8");
    console.log("DONE", el.name);
  })
  console.log("======= FINISH =======");
  return dataProcessed;
}

module.exports = convertNestedStructure;