'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user_pages', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      column1: {
        type: Sequelize.CHAR
      },
      category_id: {
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING(255)
      },
      descriptions: {
        type: Sequelize.STRING(255)
      },
      images: {
        type: Sequelize.STRING(255)
      },
      json_data: {
        type: Sequelize.JSONB
      },
      slug: {
        type: Sequelize.STRING(255)
      },
      index: {
        type: Sequelize.SMALLINT
      },
      created_at: {
        type: Sequelize.DATE
      },
      created_by: {
        type: Sequelize.UUID
      },
      updated_at: {
        type: Sequelize.DATE
      },
      updated_by: {
        type: Sequelize.UUID
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user_pages');
  }
};