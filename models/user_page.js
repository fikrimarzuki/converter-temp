'use strict';
module.exports = (sequelize, DataTypes) => {

  const Model = sequelize.Sequelize.Model

  class user_page extends Model {}

  user_page.init({
    column1: DataTypes.CHAR,
    category_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    descriptions: DataTypes.STRING,
    status_id: DataTypes.INTEGER,
    images: DataTypes.STRING,
    json_data: DataTypes.JSONB,
    slug: DataTypes.STRING,
    index: DataTypes.SMALLINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.UUID,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.UUID
  }, {
    sequelize,
    timestamps: false
  })

  // user_pages_pk
  return user_page;
};