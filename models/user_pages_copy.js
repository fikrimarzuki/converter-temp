'use strict';
module.exports = (sequelize, DataTypes) => {

  const Model = sequelize.Sequelize.Model

  class user_pages_copy extends Model {}

  user_pages_copy.init({
    column1: DataTypes.CHAR,
    category_id: DataTypes.UUID,
    name: DataTypes.STRING,
    descriptions: DataTypes.STRING,
    status_id: DataTypes.UUID,
    images: DataTypes.STRING,
    json_data: DataTypes.JSONB,
    slug: DataTypes.STRING,
    index: DataTypes.SMALLINT
  }, {
    sequelize,
    timestamps: false
  })
  return user_pages_copy;
};