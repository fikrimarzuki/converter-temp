const router = require("express").Router();
const userPagesRoute = require("./userPages");
const localRoute = require("./local");

router.get("/", (req, res, next) => {
  res.send("home")
})
router.use("/user_pages", userPagesRoute);
router.use("/local", localRoute);
router.get("/*", (req, res, next) => {
  res.send("NOT FOUND")
})

module.exports = router