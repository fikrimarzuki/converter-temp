const router = require("express").Router();
const LocalController = require("../controller/Local");

router.get("/checkStructure/:type", LocalController.checkStructure);

module.exports = router