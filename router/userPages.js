const router = require("express").Router();
const UserPages = require("../controller/UserPages");

router.get("/", UserPages.getAllData);
router.get("/write", UserPages.getDataLimitOffsetAndCreate);
router.get("/convert", UserPages.convertAndWrite);
router.put("/update", UserPages.convertAndUpdate);
router.post("/seeding", UserPages.seedingData);
router.get("/convert-nested", UserPages.changeStructureRowColumn);

module.exports = router